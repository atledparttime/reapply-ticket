package jp.atled.agileworks.addon.reapplyTicket;

import java.util.Date;

import jp.atled.workscape.papi.model.common.EntryListModel;
import jp.atled.workscape.papi.model.common.EntryModel;

import org.apache.commons.lang.StringUtils;

public class RequestInformation {
  final String unitcode;
  final String usercode;
  final Boolean rehearsalMode;
  final Date criterionDate;

  public RequestInformation(final EntryListModel jobArguments) throws Exception {
    this.unitcode = nullCheck(jobArguments, "applyUnitCode");
    this.usercode = nullCheck(jobArguments, "applyUserCode");
    this.rehearsalMode = reharsalCheck(jobArguments, "rehearsal");
    this.criterionDate = new Date();
  }
  
  private Boolean reharsalCheck(EntryListModel jobArguments, String rehearsal) {
    final EntryModel entry = jobArguments.get(rehearsal);
    if(entry == null){
      return false;
    }
    if(entry.getValue() == null){
      return false;
    }
    if(StringUtils.equals("on", entry.getValue().toString())){
      return true;
    }
    return false;
  }

  //null対応
  private String nullCheck(final EntryListModel jobArguments, final String key) throws Exception {
    final EntryModel entry = jobArguments.get(key);

    if (entry == null) {
      throw new Exception("ジョブ引数" + key + "が指定されていません。\n”-D " + key + "=コード”を引数に追加してください。");
    }

    if (entry.getValue() == null) {
      throw new Exception(entry.getName() + "の値がありません。\nコードを追加してください。");
    } else {
      if (StringUtils.isEmpty(entry.getValue().toString())) {
        throw new Exception(entry.getName() + "の値がありません。\nコードを追加してください。");
      }

      return entry.getValue().toString();
    }
  }
}
