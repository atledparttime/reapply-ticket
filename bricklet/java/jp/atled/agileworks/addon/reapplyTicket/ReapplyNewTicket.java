package jp.atled.agileworks.addon.reapplyTicket;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import jp.atled.workscape.papi.annotations.Procedure;
import jp.atled.workscape.papi.annotations.PapiInject;
import jp.atled.workscape.papi.model.common.EntryListModel;
import jp.atled.workscape.papi.model.common.EntryModel;
import jp.atled.workscape.papi.model.common.EnumCompareOperatorType;
import jp.atled.workscape.papi.model.common.EnumLogicalOperatorType;
import jp.atled.workscape.papi.model.common.RecordListModel;
import jp.atled.workscape.papi.model.common.RecordModel;
import jp.atled.workscape.papi.model.common.ResponseModel;
import jp.atled.workscape.papi.model.doc.DocDataEntryListModel;
import jp.atled.workscape.papi.model.doc.DocDataEntryModel;
import jp.atled.workscape.papi.model.doc.DocDataModel;
import jp.atled.workscape.papi.model.doc.DocModel;
import jp.atled.workscape.papi.model.doc.DocReferenceModel;
import jp.atled.workscape.papi.model.doc.DocResponseModel;
import jp.atled.workscape.papi.model.doc.DocViewColumnModel;
import jp.atled.workscape.papi.model.doc.EnumDocViewColumnType;
import jp.atled.workscape.papi.model.doc.FieldValueConditionListModel;
import jp.atled.workscape.papi.model.doc.FieldValueConditionModel;
import jp.atled.workscape.papi.model.doc.PrepareDocRequestModel;
import jp.atled.workscape.papi.model.doc.SelectDocConditionModel;
import jp.atled.workscape.papi.model.doc.SelectDocRequestModel;
import jp.atled.workscape.papi.model.plugin.BrickletCallRequestModel;
import jp.atled.workscape.papi.model.plugin.BrickletCallResponseModel;
import jp.atled.workscape.papi.model.workflow.EnumWorkflowStateType;
import jp.atled.workscape.papi.model.workflow.RejectRequestModel;
import jp.atled.workscape.papi.model.workflow.StartRequestModel;
import jp.atled.workscape.papi.model.workflow.WorkflowInfoModel;
import jp.atled.workscape.papi.service.doc.DocService;
import jp.atled.workscape.papi.service.logging.BrickletLogger;
import jp.atled.workscape.papi.service.system.SystemManager;
import jp.atled.workscape.papi.service.system.Transaction;
import jp.atled.workscape.papi.service.workflow.WorkflowService;

public class ReapplyNewTicket {

  @PapiInject
  DocService docService;

  @PapiInject
  WorkflowService workflowService;

  @PapiInject
  SystemManager systemManager;

  @PapiInject
  BrickletLogger logger;

  final static String FOME_CODE_CHANGE = "aw_change_sheet2";
  final static String RULE_CODE_CHANGE = "aw_change_sheet2";
  final static String CURRENT_STATUS = "current_status";
  final static String INVEST_RESULT = "invest_result";

  @Procedure
  public void reapplyTicket(BrickletCallRequestModel request, BrickletCallResponseModel response) throws SQLException {
    final EntryListModel paramList = request.getParameterList();
    try {
      if (paramList.isEmpty()) {
        throw new Exception("ジョブ引数が指定されていません。");
      }
      final List<DocModel> listOldTickets = getOldTicketList(searchOldTickets());
      if (listOldTickets.isEmpty()) {
        printInformationMessage("対象の書類はありません。");
      } else {
        printSearchResult(listOldTickets);
        final RequestInformation requestInformation = new RequestInformation(paramList);
        final List<ResultNewTicket> listNewTicket = run(requestInformation, listOldTickets);
        printResultApplyNewTicket(listNewTicket);
      }
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
    }
  }

  //新規書類再申請リスト
  private void printResultApplyNewTicket(final List<ResultNewTicket> listNewTickets) {
    if (listNewTickets.isEmpty()) {
      printInformationMessage("再申請した書類はありません。");
    } else {
      printInformationMessage("再申請実行結果");
      for (ResultNewTicket newTicket : listNewTickets) {
        printInformationMessage(newTicket.message());
      }
    }
  }

  //oldTicketとジョブ引数をもとに処理する。
  private List<ResultNewTicket> run(final RequestInformation requestInformation, final List<DocModel> listOldTickets) throws SQLException {
    final List<ResultNewTicket> listResultNewTickets = new ArrayList<ResultNewTicket>(listOldTickets.size());
    final Transaction transaction = systemManager.getDatabaseHandler().transaction();
    for (DocModel oldTicket : listOldTickets) {
      try {
        transaction.start();
        final String investResult = nullCheck(oldTicket.getDocData().getFieldData(INVEST_RESULT)).toString();
        NewTicketSheet newTicketSheet = NewTicketSheet.getEnumNewTicketsheet(investResult);
        //新チケット申請
        final DocModel newTicket = applyNewTickets(oldTicket, newTicketSheet, requestInformation);
        final WorkflowInfoModel workflow = workflowService.getWorkflowInfo(oldTicket.getId()).getWorkflowInfo();
        rejectOldTicket(oldTicket, workflow);
        //成功した新チケットをリストに追加。
        listResultNewTickets.add(new ResultNewTicket(newTicketSheet.getSheetName(), newTicket.getId(), oldTicket.getId()));
        if (requestInformation.rehearsalMode) {//リハーサルモードがonであれば申請、却下を取り消す
          transaction.rollback();
          continue;
        }
        transaction.commit();
      } catch (ErrorMessage e) {
        printErrorMessage(e.getMessage(), e);
        listResultNewTickets.add(new ResultNewTicket(oldTicket.getId()));
        transaction.rollback();
      } catch (Exception e) {
        printErrorMessage(e.getMessage(), e);
        listResultNewTickets.add(new ResultNewTicket(oldTicket.getId()));
        transaction.rollback();
      }
    }
    if (listResultNewTickets.isEmpty()) {
      return Collections.emptyList();
    } else {
      return listResultNewTickets;
    }
  }

  //旧チケットのフィールドのデータオブジェクトのnull対応
  private Object nullCheck(Object fieldData) {
    if (fieldData == null) {
      //確認結果がnullである場合は未選択なので書類申請を続行
      return "";
    }
    return fieldData;
  }

  //新規シートで書類を申請する
  private DocModel applyNewTickets(final DocModel oldTicket, final NewTicketSheet newTicketSheet, final RequestInformation requestInformation) throws ErrorMessage, Exception {
    final DocModel prepareDoc = prepareNewTicketDocument(oldTicket, newTicketSheet, requestInformation);
    copyDocData(prepareDoc.getDocData(), oldTicket.getDocData());
    final DocModel newTicket = saveNewTicketDocument(prepareDoc, oldTicket, newTicketSheet);
    addDocReference(oldTicket, newTicket);
    startNewTicket(newTicket, newTicketSheet, requestInformation);
    return newTicket;
  }

  //旧チケットを却下する
  private void rejectOldTicket(final DocModel oldTicket, final WorkflowInfoModel workflow) {
    final RejectRequestModel rejectRequest = docService.createRequestModel(RejectRequestModel.class);
    rejectRequest.setDocId(oldTicket.getId());
    rejectRequest.setComment("チケット再申請のため却下");
    rejectRequest.setOperatorCode("awr");
    rejectRequest.setRuleStepCode(workflow.getCurrentRuleStepHeaderList().get(0).getCode());
    _c(workflowService.reject(rejectRequest));
  }

  //製品指摘管理シートを製品指摘管理ルールで申請
  private void startNewTicket(final DocModel newTicket, final NewTicketSheet newTicketSheet, final RequestInformation requestInformation) {
    final StartRequestModel startRequest = docService.createRequestModel(StartRequestModel.class);
    startRequest.setCriterionDate(requestInformation.criterionDate);
    startRequest.setDocId(newTicket.getId());
    startRequest.setFormCode(newTicketSheet.getFormCode());
    startRequest.setRuleCode(newTicketSheet.getRule());
    startRequest.setApplyUnitCode(requestInformation.unitcode);
    startRequest.setApplyUserCode(requestInformation.usercode);
    _c(workflowService.start(startRequest));
  }

  //関連書類情報を追加する
  private void addDocReference(final DocModel oldTicket, final DocModel newTicket) {
    final DocReferenceModel docReference = docService.createRequestModel(DocReferenceModel.class);
    docReference.setDocId(newTicket.getId());
    docReference.setRefDocId(oldTicket.getId());
    _c(docService.addDocReference(docReference));
  }

  //新しい書類を準備する
  private DocModel prepareNewTicketDocument(final DocModel oldTicket, final NewTicketSheet newTicketSheet, final RequestInformation requestInformation) {
    final PrepareDocRequestModel prepareDocRequest = docService.createRequestModel(PrepareDocRequestModel.class);
    prepareDocRequest.setCriterionDate(requestInformation.criterionDate);
    prepareDocRequest.setFormCode(newTicketSheet.getFormCode());
    prepareDocRequest.setRuleCode(newTicketSheet.getRule());
    prepareDocRequest.setApplyUnitCode(requestInformation.unitcode);
    prepareDocRequest.setApplyUserCode(requestInformation.usercode);
    return _c(docService.prepareDocRequest(prepareDocRequest)).getDoc();
  }

  //新チケットに転記した内容を保存する
  private DocModel saveNewTicketDocument(final DocModel prepareDoc, final DocModel oldTicket, final NewTicketSheet newTicketSheet) throws ErrorMessage {
    //要望シートでは以下のフィールドが必須項目のため未入力の場合はBrickletCallResponseModelにエラーを投げる
    if (newTicketSheet == NewTicketSheet.REQUEST) {
      if (prepareDoc.getDocData().getFieldData("title") == null) {
        throw new ErrorMessage("ERR001", oldTicket.getId(), "件名(title)");
      }
      if (prepareDoc.getDocData().getFieldData("rep_contents") == null) {
        throw new ErrorMessage("ERR002", oldTicket.getId(), "報告内容(rep_contents)");
      }
    }
    return _c(docService.addDoc(prepareDoc)).getDoc();
  }

  //転記する
  private void copyDocData(final DocDataModel prepareDocData, final DocDataModel oldDocData) {
    final DocDataEntryListModel prepareDocDataEntryList = prepareDocData.getEntryList();
    final Map<String, DocDataEntryModel> fieldDataMapOldTicket = getFieldDataMap(oldDocData.getEntryList());
    for (DocDataEntryModel pEntry : prepareDocDataEntryList) {
      if (fieldDataMapOldTicket.containsKey(pEntry.getFieldId())) {
        final String fieldId = pEntry.getFieldId();
        if (prepareDocData.getGroupRowCount(fieldId) == 0) {//普通のフィールドかどうか
          prepareDocData.setFieldData(fieldId, oldDocData.getFieldData(fieldId));
          continue;
        }
        copyGroupData(pEntry, fieldDataMapOldTicket.get(fieldId).getDataValue().getEntryList(), prepareDocData, oldDocData);
      }
    }
  }

  /**グループデータの転記
   * @param pEntry
   * @param oldGroupData
   * @param prepareDocData
   * @param oldDocData
   */
  private void copyGroupData(final DocDataEntryModel pEntry, final DocDataEntryListModel oldGroupData, final DocDataModel prepareDocData, final DocDataModel oldDocData) {
    final String groupId = pEntry.getFieldId();
    final int maxRowCount = prepareDocData.getGroupRowCount(groupId);
    final Map<String, DocDataEntryModel> groupDataMapOldTicket = getFieldDataMap(oldGroupData);
    final DocDataEntryListModel prepareGroupDataEntryList = pEntry.getDataValue().getEntryList();

    for (int i = 0; i < maxRowCount; i++) {
      for (DocDataEntryModel prepareGroupData : prepareGroupDataEntryList) {
        final String fieldId = prepareGroupData.getFieldId();
        if (groupDataMapOldTicket.containsKey(fieldId)) {
          prepareDocData.setGroupData(groupId, fieldId, i, oldDocData.getGroupData(groupId, fieldId, i));
        }
      }
    }
  }

  //旧チケットのフィールドデータをマップとして取得
  private Map<String, DocDataEntryModel> getFieldDataMap(final DocDataEntryListModel docDataEntryList) {
    final Map<String, DocDataEntryModel> fieldDataMap = new HashMap<String, DocDataEntryModel>(docDataEntryList.size());
    for (DocDataEntryModel docDataEntry : docDataEntryList) {
      fieldDataMap.put(docDataEntry.getFieldId(), docDataEntry);
    }
    return fieldDataMap;
  }

  //旧チケット検索
  private RecordListModel searchOldTickets() {
    final SelectDocRequestModel selectDocRequest = docService.createRequestModel(SelectDocRequestModel.class);
    setDocSearchCondition(selectDocRequest);
    setDocViewColumn(selectDocRequest);
    return _c(docService.selectDoc(selectDocRequest)).getResultSet().getRecordList();
  }

  //マスタの検索結果項目を設定（書類ID）
  private void setDocViewColumn(final SelectDocRequestModel selectDocRequest) {
    selectDocRequest.getDocView().setFormCode(FOME_CODE_CHANGE);
    final DocViewColumnModel docViewColumnOfDocId = selectDocRequest.getDocView().getColumnList().add();
    docViewColumnOfDocId.setType(EnumDocViewColumnType.DOC_ID);
    final DocViewColumnModel docViewColumnCurentStatus = selectDocRequest.getDocView().getColumnList().add();
    docViewColumnCurentStatus.setFieldId(CURRENT_STATUS);
  }

  //検索条件指定
  private void setDocSearchCondition(final SelectDocRequestModel selectDocRequest) {
    final SelectDocConditionModel condition = selectDocRequest.getCondition();
    condition.setFormCode(FOME_CODE_CHANGE);
    condition.setRuleCode(RULE_CODE_CHANGE);
    condition.setWorkflowState(EnumWorkflowStateType.ACTIVE);
    condition.setLogicalOperatorType(EnumLogicalOperatorType.OR);
    final FieldValueConditionListModel currentStatusConditionList = condition.getFieldValueConditionList();
    final FieldValueConditionModel currentStatusZikai = currentStatusConditionList.add();
    currentStatusZikai.setCompareOperatorType(EnumCompareOperatorType.EQUAL);
    currentStatusZikai.setFieldId(CURRENT_STATUS);
    currentStatusZikai.setValue("次回検討待ち");
    final FieldValueConditionModel currentStatusYoubou = currentStatusConditionList.add();
    currentStatusYoubou.setCompareOperatorType(EnumCompareOperatorType.EQUAL);
    currentStatusYoubou.setFieldId(CURRENT_STATUS);
    currentStatusYoubou.setValue("要望検討待ち");
  }

  //待ちステップの旧チケットのリストを生成
  private List<DocModel> getOldTicketList(final RecordListModel oldTickets) {
    final List<DocModel> matchiTickets = new ArrayList<DocModel>(oldTickets.size());
    for (RecordModel record : oldTickets) {
      final EntryModel docId = record.getEntryList().get(0);
      DocResponseModel docResponse = _c(docService.getDoc(Long.valueOf(docId.getValue().toString())));
      matchiTickets.add(docResponse.getDoc());
    }
    if (matchiTickets.isEmpty()) {
      return Collections.emptyList();
    } else {
      return matchiTickets;
    }
  }

  /**
   * 検索した結果を表示する
   * @param resultList
   * @return
   */
  private void printSearchResult(final List<DocModel> listOldTickets) {
    if (logger.isInfoEnabled()) {
      logger.info("旧チケットの検索結果");
      logger.info("書類ID:現在ステータス:確認結果");
      StringBuilder infoSentence = new StringBuilder(128);
      for (DocModel ticket : listOldTickets) {
        logger.info(joinResultRecordEntryDataToSentence(infoSentence, ticket));
        infoSentence.setLength(0);
      }
    }
  }

  /**
   * 文字列連結
   * @param infoSentence
   * @param doc
   */
  private String joinResultRecordEntryDataToSentence(final StringBuilder infoSentence, final DocModel doc) {
    infoSentence.append(doc.getId().toString());
    infoSentence.append(':');
    final DocDataModel docData = doc.getDocData();
    if (docData.getFieldData(CURRENT_STATUS) != null) {
      infoSentence.append(doc.getDocData().getFieldData(CURRENT_STATUS).toString());
    }
    infoSentence.append(':');
    final Object fieldDataInvestResult = docData.getFieldData(INVEST_RESULT);
    if (fieldDataInvestResult == null) {
      infoSentence.append("選択なし");
    } else if (StringUtils.isEmpty(fieldDataInvestResult.toString())) {
      infoSentence.append("選択なし");
    } else {
      infoSentence.append(fieldDataInvestResult.toString());
    }
    return infoSentence.toString();
  }

  //インフォメーションメッセージを出力
  private void printInformationMessage(final String messageTextString) {
    if (logger.isInfoEnabled()) {
      logger.info(messageTextString);
    }
  }

  //エラーメッセージを出力
  private void printErrorMessage(final String messageTextString, final Throwable e) {
    if (logger.isDebugEnabled()) {
      logger.error(messageTextString, e);
    }
  }

  /**レスポンスエラーチェック
   * @param response
   * @return
   */
  private <R extends ResponseModel> R _c(final R response) {
    if (!response.getResultStatus().isSuccess()) {
      if (logger.isDebugEnabled()) {
        logger.error(response.getResultStatus().toString());
      }
      throw new PapiRuntimeException(response.getResultStatus());
    }
    return response;
  }
}
