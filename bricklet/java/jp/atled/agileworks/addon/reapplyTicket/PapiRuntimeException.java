package jp.atled.agileworks.addon.reapplyTicket;

import jp.atled.workscape.papi.model.common.ResultStatusModel;

public class PapiRuntimeException extends RuntimeException {
  private ResultStatusModel resultStatus;

  public PapiRuntimeException(final ResultStatusModel resultStatus) {
    super(resultStatus.getCode() + ":" + resultStatus.getText());
    this.resultStatus = resultStatus;
  }

  public ResultStatusModel getResultStatus() {
    return resultStatus;
  }
}
