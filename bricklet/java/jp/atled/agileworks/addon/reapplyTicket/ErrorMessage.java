package jp.atled.agileworks.addon.reapplyTicket;

public class ErrorMessage extends Exception {
  final String code;
  final String fieldId;

  public ErrorMessage(final String code, final Long tickteId, final String fieldId) {
    super("書類ID：" + Long.toString(tickteId) + "の" + fieldId + "が無記入のため再申請に失敗しました。");
    this.code = code;
    this.fieldId = fieldId;
  }

  String getErrorText() {
    return "要望管理シートでは" + fieldId + "は必須項目です。";
  }
}
