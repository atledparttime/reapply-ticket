package jp.atled.agileworks.addon.reapplyTicket;

import org.apache.commons.lang.StringUtils;

public class ResultNewTicket {
  final String sheetName;
  final Long id;
  final Long oldTicketId;

  public ResultNewTicket(final String invest, final Long id, final Long old) {
    sheetName = invest;
    this.id = id;
    oldTicketId = old;
  }

  public ResultNewTicket(final Long old) {
    sheetName = null;
    id = null;
    oldTicketId = old;
  }

  public String message() {
    if (StringUtils.isEmpty(sheetName)) {
      return "失敗(旧チケットID:" + oldTicketId.toString()+')';
    } else {
      return "成功[" + sheetName + "] (新チケットID:" + id.toString() + ") , 却下(旧チケットID:" + oldTicketId.toString() + ')';
    }
  }

}
