package jp.atled.agileworks.addon.reapplyTicket;

import org.apache.commons.lang.StringUtils;

public enum NewTicketSheet {
    BUG {
      @Override
      public String getFormCode() {
        return "aw_bug_sheet2";
      }

      @Override
      public String getRule() {
        return "aw_bug_sheet2_reapply";
      }

      @Override
      public String getSheetName() {
        return "製品指摘管理シート";
      }
    },

    REQUEST {
      @Override
      public String getFormCode() {
        return "aw_request_sheet2";
      }

      @Override
      public String getRule() {
        return "aw_request_sheet2_reapply";
      }

      @Override
      public String getSheetName() {
        return "要望管理シート";
      }
    };

    public static NewTicketSheet getEnumNewTicketsheet(String investResult){
      if (StringUtils.equals(investResult, "要望")){
        return REQUEST;
      }
      return BUG;
    };

    public abstract String getFormCode();
    public abstract String getRule();
    public abstract String getSheetName();
}
